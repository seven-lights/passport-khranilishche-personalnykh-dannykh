<?php

use yii\db\Schema;
use yii\db\Migration;

class m150329_101944_add_sex_in_passport_table extends Migration
{
    public function up()
    {
	    $this->addColumn('passport', 'sex', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m150329_101944_add_sex_in_passport_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
