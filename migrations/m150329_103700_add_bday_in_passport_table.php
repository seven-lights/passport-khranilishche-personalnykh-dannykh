<?php

use yii\db\Schema;
use yii\db\Migration;

class m150329_103700_add_bday_in_passport_table extends Migration
{
    public function up()
    {
	    $this->addColumn('passport', 'birthday', Schema::TYPE_BIGINT);
    }

    public function down()
    {
        echo "m150329_103700_add_bday_in_passport_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
