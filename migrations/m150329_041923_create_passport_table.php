<?php

use yii\db\Schema;
use yii\db\Migration;

class m150329_041923_create_passport_table extends Migration
{
    public function up()
    {
	    $this->createTable('passport', [
		    'id' => Schema::TYPE_PK,
		    'first_name' => Schema::TYPE_STRING . '(20) NOT NULL',
		    'last_name' => Schema::TYPE_STRING . '(20) NOT NULL',
		    'father_name' => Schema::TYPE_STRING . '(20)',
		    'phone' => Schema::TYPE_STRING . '(20)',
		    'email' => Schema::TYPE_STRING . '(50)',
		    'user_id' => Schema::TYPE_INTEGER,
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->createIndex('passport_tbl_phone_idx', 'passport', 'phone', true);
	    $this->createIndex('passport_tbl_email_idx', 'passport', 'email', true);
	    $this->createIndex('passport_tbl_user_id_idx', 'passport', 'user_id', true);
    }

    public function down()
    {
        echo "m150329_041923_create_passport_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
