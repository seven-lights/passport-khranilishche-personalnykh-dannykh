<?php

use yii\db\Schema;
use yii\db\Migration;

class m150329_042002_create_passport_additional_phone_table extends Migration
{
    public function up()
    {
	    $this->createTable('passport_additional_phone', [
		    'passport_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'phone' => Schema::TYPE_STRING . '(20) NOT NULL',
	    ]);
	    $this->createIndex(
		    'passport_additional_phone_tbl_passport_id_phone_idx',
		    'passport_additional_phone',
		    ['passport_id', 'phone'],
		    true
	    );
	    $this->addForeignKey(
		    'passport_id_FK_passport_additional_phone',
		    'passport_additional_phone',
		    'passport_id',
		    'passport',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );
    }

    public function down()
    {
        echo "m150329_042002_create_passport_additional_phone_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
